## Local development

Before you start your journey with the application, please create a copy
of `application-core/src/main/resources/application-local.template.yml`
file, name it `application-core/src/main/resources/application-local.yml` - this one is ignored by git by default and
serves as your private local configuration. You don't have to change the most of the properties in a template file,
unless you have some custom setup.

## Running application

Just execute the main method in `LegaEntityApiApplication` class, don't forget to set spring profile to `local` (to
pick up `application-local.yml` file)

If needed local MySQL database can be run from dockerfile:
```
cd docker
docker-compose -f docker-compose.mysql.yml up -d
```

Remember to set database name, user, and password to `ework_it_test`
and port to `3367` in your application-local.yml


## Releases

Normally, when you merge your PR or push to master,
the [axion-release-plugin](https://axion-release-plugin.readthedocs.io/en/latest/)
will bump [semver](https://semver.org/) version `patch` part during the `build:master` gitlab job.

If you want to release next minor or major version, just include `[release]` (for minor) or `[major release]` (for
major)
in your commit message.

Versions are only released from `master` branch

During the gitlab pipeline for `master` branch, dependent packages like `integration-dto` will be published to our S3
artifact repository, you don't need to call `publish` goal

## Health checks

For k8s probes, please use following endpoints:

* `GET /api/actuator/health/readiness`
* `GET /api/actuator/health/liveness`
