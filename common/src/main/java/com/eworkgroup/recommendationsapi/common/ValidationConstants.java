package com.eworkgroup.recommendationsapi.common;

import lombok.experimental.UtilityClass;

@UtilityClass
public class ValidationConstants {
    public static final int DEFAULT_MAX_CHARS = 255;
    public static final int PHONE_CODE_CHARS = 2;
    public static final int PIN_MAX_CHARS = 10;
    public static final int TOKEN_MAX_CHARS = 32;
    public static final int MOTIVATION_MAX_CHARS = 5000;
    public static final int FEEDBACK_COMMENT_MAX_CHARS = 2000;
    public static final int FILE_MAX_NAME_CHARS = 2000;
    public static final int URL_CHARS = 2000;
    public static final int HTML_MAX_CHARS = 15000;
    public static final int EMAIL_MAX_CHARS = 100;
    public static final int PHONE_MAX_CHARS = 50;
    public static final int ZIP_CODE_MAX_CHARS = 50;
    public static final int VAT_NUMBER_CHARS = 25;
}
