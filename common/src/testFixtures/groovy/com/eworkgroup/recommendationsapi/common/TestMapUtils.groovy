package com.eworkgroup.recommendationsapi.common

import java.util.function.Function

class TestMapUtils {

    static <R> R getProp(Map<String, R> map, String key, Function<Map, R> defaultValue) {
        return map.containsKey(key) ? map[key] : defaultValue.apply(map)
    }

    static <R> Map<String, R> getSubmap(Map<String, R> map, String key) {
        return map.findAll { it.key.startsWith("$key.") }
                .collectEntries { [it.key.replaceFirst("$key\\.", ''), it.value] }
    }

}
