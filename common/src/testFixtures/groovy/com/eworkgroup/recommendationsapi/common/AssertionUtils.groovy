package com.eworkgroup.recommendationsapi.common

import java.util.function.BiConsumer
import java.util.function.Function

class AssertionUtils {

    static <T, Y> void assertEqualCollectionElements(Collection<Y> expected, Collection<T> actual, String keyField, BiConsumer<Y, T> assertion) {
        assertEqualCollectionElements(expected, actual, { y -> y[keyField] }, { t -> t[keyField] }, assertion)
    }

    static <T, Y, Z> void assertEqualCollectionElements(Collection<Y> expected, Collection<T> actual, Function<Y, Z> expectedKeyExtractor, Function<T, Z> actualKeyExtractor, BiConsumer<Y, T> assertion) {
        assert expected.size() == actual.size()
        Map<Z, T> actualMap = actual.collectEntries { actualItem -> [actualKeyExtractor.apply(actualItem), actualItem] }
        for (Y expectedItem : expected) {
            def actualItem = actualMap[expectedKeyExtractor.apply(expectedItem)]
            assertion.accept(expectedItem, actualItem)
        }
    }

}
