package com.eworkgroup.recommendationsapi.common

import org.apache.commons.lang3.RandomStringUtils

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.temporal.ChronoUnit

class RandomTestUtils {

    private static final Random random = new Random()

    static String randomString(int minLength = 20, int maxLength = 20) {
        return RandomStringUtils.randomAlphanumeric(minLength, maxLength)
    }

    static String randomEmail() {
        return randomString() + "@test.eworkgroup.com"
    }

    static Long randomLong(long min = 1, long max = Long.MAX_VALUE) {
        return (min + Math.abs(random.nextLong())) % max
    }

    static int randomInt(int min = 1, int max = Integer.MAX_VALUE) {
        return (min + Math.abs(random.nextInt())) % max
    }

    static String randomPhoneNumber() {
        return "${randomLong(500_000_000, 999_999_999)}"
    }

    static boolean randomBoolean() {
        random.nextBoolean()
    }

    static <T> T random(T[] values) {
        return values[random.nextInt(values.length)]
    }

    static <T> T random(Collection<T> values) {
        return values[random.nextInt(values.size())]
    }

    static LocalDate randomFutureDate(LocalDate startDate = LocalDate.now()) {
        startDate.plusDays(randomInt(100, 300))
    }

    static LocalDateTime randomFutureDatetime(LocalDateTime startDatetime = now()) {
        startDatetime.plusHours(randomInt(200, 500))
    }

    static Currency randomCurrency() {
        random(Currency.availableCurrencies)
    }

    static BigDecimal randomBigDecimal(min, max) {
        BigDecimal.valueOf(random.ints(min, (max + 1)).findFirst().getAsInt())
    }

    private static LocalDateTime now() {
        LocalDateTime.now().truncatedTo(ChronoUnit.MILLIS)
    }

}
