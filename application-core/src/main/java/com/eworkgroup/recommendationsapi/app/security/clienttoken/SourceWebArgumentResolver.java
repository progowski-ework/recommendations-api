package com.eworkgroup.recommendationsapi.app.security.clienttoken;

import com.eworkgroup.recommendationsapi.app.security.UnauthorizedException;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class SourceWebArgumentResolver implements HandlerMethodArgumentResolver {

    private final TokenAuthorizer tokenAuthorizer;

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.getParameterType().equals(Source.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer, NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        String authorizationToken = Optional.ofNullable(webRequest.getHeader(HttpHeaders.AUTHORIZATION))
                .filter(StringUtils::isNotBlank)
                .map(this::stripBearer)
                .orElseThrow(() -> new UnauthorizedException("Missing or malformed Authorization header"));

        return tokenAuthorizer.authorize(authorizationToken);
    }

    private String stripBearer(String headerValue) {
        return StringUtils.removeStartIgnoreCase(headerValue, "Bearer ");
    }


}
