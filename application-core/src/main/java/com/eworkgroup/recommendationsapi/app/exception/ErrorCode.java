package com.eworkgroup.recommendationsapi.app.exception;

public enum ErrorCode {
    UNKNOWN,
    FIELD_ERROR,
    UNAUTHORIZED
}
