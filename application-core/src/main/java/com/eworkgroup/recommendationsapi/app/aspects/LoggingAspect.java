package com.eworkgroup.recommendationsapi.app.aspects;

import com.eworkgroup.recommendationsapi.app.exception.ApplicationExceptionHandler;
import com.eworkgroup.recommendationsapi.app.security.clienttoken.Source;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Aspect
@Component
@Slf4j
public class LoggingAspect {

    private Map<Class, Logger> loggers = new ConcurrentHashMap<>();

    @Before("(service() || restController())")
    public void log(JoinPoint joinPoint) {
        Logger logger = loggers.computeIfAbsent(joinPoint.getTarget().getClass(), LoggerFactory::getLogger);
        if (logger.isTraceEnabled()) {
            logger.trace("Method '{}' called with parameters: {}", joinPoint.getSignature().getName(), Arrays.deepToString(joinPoint.getArgs()));
        }
    }

    @Pointcut("@within(org.springframework.stereotype.Service)")
    public void service() {
        // not logged yet
    }

    @Pointcut("@within(org.springframework.web.bind.annotation.RestController)")
    public void restController() {
        // not logged yet
    }

    @Before("restController() && args(.., source)")
    public void beforeRestController(JoinPoint joinPoint, Source source) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes())
                    .getRequest();
            String url = requestPath(request);
            log.info("{} {} {}", source, request.getMethod(), url);
        } catch (IllegalStateException e) {
            // ignored (called controller method outside of dispatcher servlet i.e. manually or during the tests)
        }
    }

    private String requestPath(HttpServletRequest request) {
        String queryString = request.getQueryString();
        if (queryString != null) {
            return request.getRequestURI() + "?" + queryString;
        }
        return request.getRequestURI();
    }

    @AfterReturning("restController() && !execution(* *clearAllCaches(..))")
    public void afterReturning(JoinPoint joinPoint) {
        try {
            HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
            log.info(HttpStatus.OK.toString(), request);
        } catch (IllegalStateException e) {
            // ignored (called controller method outside of dispatcher servlet i.e. manually or during the tests)
        }
    }

    @AfterThrowing(pointcut = "restController() && !execution(* *clearAllCaches(..))", throwing = "ex")
    public void afterThrowing(JoinPoint joinPoint, Exception ex) {
        HttpStatus responseStatus = getStatusForException(ex);
        log.info(ex.getMessage(), responseStatus);
    }

    private HttpStatus getStatusForException(Exception ex) {
        List<Method> allMethods = Arrays.asList(ApplicationExceptionHandler.class.getDeclaredMethods());
        return allMethods.stream()
                .filter(method -> method.isAnnotationPresent(ExceptionHandler.class))
                .filter(method -> Arrays.asList(method.getAnnotation(ExceptionHandler.class).value()).contains(ex.getClass()))
                .map(method -> method.getAnnotation(ResponseStatus.class).value())
                .findAny()
                .orElse(HttpStatus.BAD_REQUEST);
    }
}
