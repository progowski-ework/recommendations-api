package com.eworkgroup.recommendationsapi.app.exception;

public class NotFoundException extends RuntimeException {

    private static final String NOT_FOUND_WITH_ID = "Couldn't find entity with id = ";
    public static final String NOT_FOUND = "Couldn't find entity";

    public NotFoundException(Long id) {
        super(NOT_FOUND_WITH_ID + id);
    }

    public NotFoundException(String id) {
        super(NOT_FOUND_WITH_ID + id);
    }

    public NotFoundException() {
        super(NOT_FOUND);
    }
}
