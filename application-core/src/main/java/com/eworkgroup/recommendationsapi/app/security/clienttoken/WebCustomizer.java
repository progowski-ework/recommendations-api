package com.eworkgroup.recommendationsapi.app.security.clienttoken;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class WebCustomizer implements WebMvcConfigurer {

    private final SourceWebArgumentResolver sourceWebArgumentResolver;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> resolvers) {
        resolvers.add(sourceWebArgumentResolver);
    }
}
