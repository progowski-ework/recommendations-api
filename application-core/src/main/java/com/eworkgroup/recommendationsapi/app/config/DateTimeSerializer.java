package com.eworkgroup.recommendationsapi.app.config;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.fasterxml.jackson.datatype.jsr310.ser.InstantSerializer;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class DateTimeSerializer extends StdSerializer<LocalDateTime> {

    private final transient JsonSerializer<Instant> destinationSerializer;
    private final TimeZone timeZone;

    public DateTimeSerializer(TimeZone timeZone) {
        super(LocalDateTime.class);
        this.destinationSerializer = InstantSerializer.INSTANCE;
        this.timeZone = timeZone;
    }

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator gen, SerializerProvider provider) throws IOException {
        Instant instant = localDateTime.atZone(timeZone.toZoneId()).toInstant();
        destinationSerializer.serialize(instant, gen, provider);
    }
}


