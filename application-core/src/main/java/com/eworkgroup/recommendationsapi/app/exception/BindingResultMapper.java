package com.eworkgroup.recommendationsapi.app.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Component
public class BindingResultMapper {

    public ValidationErrors toErrors(BindingResult bindingResult) {
        ValidationErrors errors = new ValidationErrors();
        if (!bindingResult.hasErrors()) {
            return errors;
        }
        errors.setFieldErrors(bindingResult.getFieldErrors().stream()
                .map(error -> new FieldErrorDto(error.getField(), error.getCode()))
                .collect(toList()));
        errors.setGeneralErrors(bindingResult.getGlobalErrors().stream()
                .map(DefaultMessageSourceResolvable::getCode)
                .collect(toList()));
        return errors;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class ValidationErrors {
        List<String> generalErrors;
        List<FieldErrorDto> fieldErrors;
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    static class FieldErrorDto {
        private String field;
        private String error;
    }

}
