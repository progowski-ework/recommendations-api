package com.eworkgroup.recommendationsapi.app.security.clienttoken;

import com.eworkgroup.recommendationsapi.app.security.UnauthorizedException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class TokenAuthorizer {

    private final SourceAuthorizationProperties authorizationProperties;

    public Source authorize(String token) {
        return authorizationProperties.getSourceByToken(token)
                .orElseThrow(() -> new UnauthorizedException("No source defined for given authorization token"));
    }

}
