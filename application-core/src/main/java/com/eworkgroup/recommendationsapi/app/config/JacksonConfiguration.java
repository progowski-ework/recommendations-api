package com.eworkgroup.recommendationsapi.app.config;


import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.TimeZone;

@Configuration
public class JacksonConfiguration {

    @Bean
    public Jackson2ObjectMapperBuilderCustomizer jsonCustomizer(@Value("${ework.time-zone:UTC}") TimeZone timeZone) {
        return builder -> builder.serializers(LocalDateSerializer.INSTANCE)
                .deserializers(LocalDateDeserializer.INSTANCE)
                .serializers(new DateTimeSerializer(timeZone))
                .deserializers(new DateTimeDeserializer(timeZone))
                .featuresToDisable(
                        SerializationFeature.WRITE_DATES_AS_TIMESTAMPS,
                        DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
                        SerializationFeature.FAIL_ON_UNWRAPPED_TYPE_IDENTIFIERS
                )
                .failOnUnknownProperties(false);
    }
}
