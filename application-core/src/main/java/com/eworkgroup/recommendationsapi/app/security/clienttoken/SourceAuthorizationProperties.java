package com.eworkgroup.recommendationsapi.app.security.clienttoken;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.Optional;

@ConfigurationProperties(prefix = "ework.authorization")
@Data
public class SourceAuthorizationProperties {

    private Map<Source, String> authorizationTokens;

    public Optional<Source> getSourceByToken(String token) {
        return authorizationTokens.entrySet().stream()
                .filter(e -> e.getValue().equals(token))
                .map(Map.Entry::getKey)
                .findFirst();
    }

}
