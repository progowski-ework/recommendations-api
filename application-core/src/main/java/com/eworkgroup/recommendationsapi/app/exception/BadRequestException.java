package com.eworkgroup.recommendationsapi.app.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public class BadRequestException extends RuntimeException {

    private final ErrorCode errorCode;
    private final Object value;

    @Override
    public String getMessage() {
        return "EworkException, code = " + errorCode;
    }
}
