package com.eworkgroup.recommendationsapi.app.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.auditing.DateTimeProvider;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.time.Clock;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Configuration
@EnableJpaAuditing(dateTimeProviderRef = "currentDateTimeProvider")
public class JpaConfiguration {

    @Bean
    public DateTimeProvider currentDateTimeProvider(Clock clock) {
        return () -> Optional.of(LocalDateTime.now(clock).truncatedTo(ChronoUnit.MILLIS));
    }

}
