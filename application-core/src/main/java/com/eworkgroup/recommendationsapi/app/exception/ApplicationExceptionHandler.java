package com.eworkgroup.recommendationsapi.app.exception;

import com.eworkgroup.recommendationsapi.app.security.UnauthorizedException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.UUID;

@Slf4j
@RestControllerAdvice
@RequiredArgsConstructor
public class ApplicationExceptionHandler {

    private final BindingResultMapper bindingResultMapper;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler
    public ErrorInfo handleGenericException(Exception e) {
        UUID uuid = UUID.randomUUID();
        log.error("Unhandled exception uuid: {}", uuid, e);
        return ErrorInfo.builder()
                .uuid(uuid.toString())
                .code(ErrorCode.UNKNOWN)
                .build();
    }

    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler
    public ErrorInfo handleSecurityException(UnauthorizedException e) {
        log.debug("Unauthorized exception", e);
        return ErrorInfo.builder()
                .code(ErrorCode.UNAUTHORIZED)
                .build();
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorInfo handleValidationException(MethodArgumentNotValidException exception) {
        log.debug("Validation error", exception);
        return ErrorInfo.builder()
                .code(ErrorCode.FIELD_ERROR)
                .value(bindingResultMapper.toErrors(exception.getBindingResult()))
                .build();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public ErrorInfo eworkException(BadRequestException e) {
        UUID uuid = UUID.randomUUID();
        log.error("RecommendationsApi exception", uuid, e);
        return ErrorInfo.builder()
                .uuid(uuid.toString())
                .code(e.getErrorCode())
                .value(e.getValue())
                .build();
    }

}
