package com.eworkgroup.recommendationsapi.app;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.TimeZone;

import static java.util.Arrays.asList;

@SpringBootApplication(scanBasePackages = "com.eworkgroup.recommendationsapi")
@ConfigurationPropertiesScan
@Slf4j
public class RecommendationsApiApplication implements ApplicationRunner {

    private static final String MIGRATION_PROFILE = "migration";

    @Autowired
    private Environment environment;

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    @Value("${ework.time-zone:UTC}")
    private TimeZone timeZone;

    @PostConstruct
    void started() {
        TimeZone.setDefault(timeZone);
        log.info("Application started");
    }

    public static void main(String[] args) {
        SpringApplication.run(RecommendationsApiApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments args) {
        if (asList(environment.getActiveProfiles()).contains(MIGRATION_PROFILE)) {
            log.info("DB schema migration finished");
            applicationContext.close();
            System.exit(0);
        }
    }

    @PreDestroy
    void shutdown() {
        log.info("Application shutting down");
    }
}
