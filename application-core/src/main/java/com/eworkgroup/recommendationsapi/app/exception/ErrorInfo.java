package com.eworkgroup.recommendationsapi.app.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
public class ErrorInfo {

    private String uuid;
    private ErrorCode code;
    private Object value;

}
