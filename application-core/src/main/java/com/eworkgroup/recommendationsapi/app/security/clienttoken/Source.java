package com.eworkgroup.recommendationsapi.app.security.clienttoken;

public enum Source {
    VERAMA_ATS, VMS, SP
}
