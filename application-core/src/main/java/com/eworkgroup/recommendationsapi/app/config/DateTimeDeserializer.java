package com.eworkgroup.recommendationsapi.app.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.TimeZone;

public class DateTimeDeserializer extends StdDeserializer<LocalDateTime> {

    private final transient JsonDeserializer<ZonedDateTime> targetDeserializer;
    private final TimeZone timeZone;

    public DateTimeDeserializer(TimeZone timeZone) {
        super(LocalDateTime.class);
        this.targetDeserializer = InstantDeserializer.ZONED_DATE_TIME;
        this.timeZone = timeZone;
    }

    @Override
    public LocalDateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        ZonedDateTime instant = targetDeserializer.deserialize(p, ctxt);
        return instant.withZoneSameInstant(timeZone.toZoneId()).toLocalDateTime();
    }
}


