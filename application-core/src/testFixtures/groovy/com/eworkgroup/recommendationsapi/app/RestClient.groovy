package com.eworkgroup.recommendationsapi.app

import com.eworkgroup.recommendationsapi.app.security.clienttoken.Source
import com.eworkgroup.recommendationsapi.app.security.clienttoken.SourceAuthorizationProperties
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.type.CollectionType
import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import groovy.transform.NamedDelegate
import groovy.transform.NamedParam
import groovy.transform.NamedVariant
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.mock.web.MockHttpServletResponse
import org.springframework.stereotype.Component
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.MvcResult
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import org.springframework.web.context.WebApplicationContext

import javax.annotation.PostConstruct
import javax.servlet.http.Cookie

@Component
class RestClient {

    @Autowired
    private WebApplicationContext context

    @Autowired
    private ObjectMapper objectMapper

    @Autowired
    private SourceAuthorizationProperties authorizationProperties

    private MockMvc client

    @PostConstruct
    void init() {
        client = MockMvcBuilders.webAppContextSetup(context).build()
    }

    Map<String, String> managementHeaders(Source source) {
        return source != null ? ['Authorization': 'Bearer ' + authorizationProperties.getAuthorizationTokens().get(source)] : [:]
    }

    Map<String, String> pinHeaders(String pin) {
        return ['Authorization': 'Bearer ' + pin]
    }

    @NamedVariant
    MockHttpServletResponse postForResponse(@NamedParam String url, @NamedDelegate RequestData requestData) {
        def response = perform(MockMvcRequestBuilders.post(url), requestData?.content, requestData?.headers, requestData?.cookies).response
        assertStatus(requestData, response)
        return response
    }

    @NamedVariant
    <T> T postForDto(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = postForResponse(url, requestData)
        return fromJson(response, resultClass)
    }

    @NamedVariant
    MockHttpServletResponse getForResponse(@NamedParam String url, @NamedDelegate RequestData requestData) {
        def response = perform(MockMvcRequestBuilders.get(url), requestData?.content, requestData?.headers, requestData?.cookies).response
        assertStatus(requestData, response)
        return response
    }

    @NamedVariant
    <T> T getForDto(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = getForResponse(url, requestData)
        return fromJson(response, resultClass)
    }

    @NamedVariant
    MockHttpServletResponse deleteForResponse(@NamedParam String url, @NamedDelegate RequestData requestData) {
        def response = perform(MockMvcRequestBuilders.delete(url), null, requestData?.headers, requestData?.cookies).response
        assertStatus(requestData, response)
        return response
    }

    @NamedVariant
    <T> T deleteForDto(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = deleteForDto(url, requestData)
        return fromJson(response, resultClass)
    }

    @NamedVariant
    MockHttpServletResponse putForResponse(@NamedParam String url, @NamedDelegate RequestData requestData) {
        def response = perform(MockMvcRequestBuilders.put(url), requestData?.content, requestData?.headers, requestData?.cookies).response
        assertStatus(requestData, response)
        return response
    }

    @NamedVariant
    <T> T putForDto(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = putForResponse(url, requestData)
        return fromJson(response, resultClass)
    }

    @NamedVariant
    <T> List<T> getForDtos(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = getForResponse(url, requestData)
        return fromJsonString(response.contentAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, resultClass))
    }

    @NamedVariant
    <T> List<T> getForPageDtos(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = getForResponse(url, requestData)
        return fromJsonString(extractPageContent(response), objectMapper.getTypeFactory().constructCollectionType(List.class, resultClass))
    }

    def static extractPageContent(MockHttpServletResponse response) {
        new JsonBuilder(new JsonSlurper().parseText(response.contentAsString).content).toString()
    }

    @NamedVariant
    <T> List<T> putForDtos(@NamedParam String url, @NamedParam Class<T> resultClass = Object, @NamedDelegate RequestData requestData) {
        def response = putForResponse(url, requestData)
        return fromJsonString(response.contentAsString, objectMapper.getTypeFactory().constructCollectionType(List.class, resultClass))
    }

    static class RequestData {
        Object content
        Map<String, String> headers
        Cookie[] cookies
        HttpStatus expectedStatus
    }

    private static void assertStatus(RequestData requestData, MockHttpServletResponse response) {
        if (requestData?.expectedStatus != null) {
            assert HttpStatus.resolve(response.status) == requestData.expectedStatus: "Returned response with status: $response.status, body: $response.contentAsString"
        } else {
            assert HttpStatus.resolve(response.status).is2xxSuccessful(): "Returned response with status: $response.status, body: $response.contentAsString"
        }
    }

    private MvcResult perform(MockHttpServletRequestBuilder requestBuilder, Object content, Map<String, String> headers = null, Cookie[] cookies = null) {
        return client.perform(requestBuilder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(asJsonString(content))
                .cookie(cookies != null && cookies.length > 0 ? cookies : [new Cookie('dummy-cookie', 'dummy-cookie')] as Cookie[])
                .headers(headers != null ? toHeaders(headers) : HttpHeaders.EMPTY))
                .andReturn()
    }

    private static HttpHeaders toHeaders(Map<String, String> headersMap) {
        def headers = new HttpHeaders()
        headersMap.forEach({ String key, String value -> headers.add(key, value) })
        return headers
    }

    private String asJsonString(final obj) {
        try {
            return objectMapper.writeValueAsString(obj)
        } catch (Exception e) {
            throw new RuntimeException(e)
        }
    }

    private <T> T fromJson(MockHttpServletResponse response, Class<T> clazz) {
        def responseBody = response.contentAsString
        if (responseBody == '') {
            return null
        }
        return objectMapper.readValue(responseBody, clazz)
    }

    def fromJsonString(String content, CollectionType collectionType) {
        objectMapper.readValue(content, collectionType)
    }

}
