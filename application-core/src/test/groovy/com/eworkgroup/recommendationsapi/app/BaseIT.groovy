package com.eworkgroup.recommendationsapi.app


import org.springframework.boot.test.context.SpringBootTest
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(
        classes = RecommendationsApiApplication,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        properties = [
                'ework.fileservice.url=http://localhost:${stubrunner.runningstubs.com.eworkgroup.file-api.port}',
        ]
)
@AutoConfigureStubRunner(
        ids = [
                "com.eworkgroup:file-api:+:stubs"
        ],
        stubsMode = StubRunnerProperties.StubsMode.CLASSPATH
)
@ActiveProfiles(['tests-running', 'integration-test'])
abstract class BaseIT extends Specification {

}
