#!/bin/sh
###############################################################################

if [ "$#" -ne "1" ]
then
    echo "Usage: $0 (start|migrate)" >&2
    exit 1
fi

CMD="$1"
ARGS="-javaagent:/newrelic.jar -Djava.security.egd=file:/dev/./urandom -Dspring.environment=${ENVIRONMENT_NAME} -Dspring.profiles.active=${ENVIRONMENT_NAME}"
case "${CMD}" in
    "start")
        java -Dspring.liquibase.enabled=false ${ARGS} $JAVA_OPTS -jar /app.jar
    ;;
    "migrate")
        java -Dspring.liquibase.enabled=true ${ARGS},migration $JAVA_OPTS -jar /app.jar
    ;;
    *)
        echo "ERROR: Unknown command ${CMD}" >&2
        exit 2
    ;;
esac